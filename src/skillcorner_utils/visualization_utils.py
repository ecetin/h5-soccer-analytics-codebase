import shutil
from pathlib import Path

import imageio.v2 as iio
from matplotlib import pyplot as plt
from mplsoccer import VerticalPitch
from tqdm import tqdm


def truncate(float_number: float, decimal_places: int):
    """This function truncates a float number to the given decimal places

    Args:
        float_number (float): The float number to be truncated
        decimal_places (int): The number of decimal places to truncate to

    Returns:
        float: The truncated float number
    """
    multiplier = 10**decimal_places
    return int(float_number * multiplier) / multiplier


def get_match_time(timestamp: int, half: int):
    """Return the running match time (start from the first half kick-off) from the
    given timestamp and half

    Args:
        timestamp (int): The timestamp of the event
        half (int): The half of the match (1 or 2)

    Returns:
        str: The match time in the format "minutes:seconds"
    """
    half_start = 0
    if half == 2:
        half_start = 45 * 60 * 1000
    seconds = int((timestamp + half_start) / 1000)
    minutes = int(seconds / 60)
    seconds = seconds % 60
    match_time = f"{minutes}:{seconds:02d}"
    return match_time


def animate(
    img_dir: Path,
    f_start: int,
    num_frames: int,
    animate_fps: int,
    delete_images: bool = False,
):
    """Animate the match from the given start frame to the given end frame by concatinating
    the already drawn images of each team per frame.

    Args:
        img_dir (Path): The directory containing the images to animate
        f_start (int): The start frame of the animation
        num_frames (int): The number of frames to animate
        animate_fps (int): The frames per second of the animation
        delete_images (bool, optional): Whether to delete the images after the animation is
        created. Defaults to False.
    """
    f_end = f_start + num_frames
    image_ids = [i for i in range(f_start, f_end)]

    animation_path = img_dir.parent / f"animation_{f_start}_to_{f_end}_.mp4"

    images = []
    for i in image_ids:
        file = img_dir / f"{i}.png"
        if file.is_file():
            images.append(file)
    with iio.get_writer(
        animation_path,
        format="FFMPEG",
        mode="I",
        fps=animate_fps,
    ) as writer:
        # Loop over the images and write them to the video after concatination
        # of the left and right images
        for index in tqdm(range(len(images))):
            im = iio.imread(images[index])
            writer.append_data(im)

    if delete_images:
        shutil.rmtree(img_dir)
        print("Images deleted...")

    print(
        f"Animation with {num_frames} frames at {animate_fps} FPS saved to {animation_path}"
    )


def plot(
    store_path,
    frame_id,
    timestamp,
    half,
    metadata_df,
    ball_df,
    max_ball_z,
    home_df,
    away_df,
    home_lineup_df,
    away_lineup_df,
    home,
    away,
    visible_area,
):
    match_time = get_match_time(timestamp, half)

    pitch_size = (
        metadata_df["pitch_width"].iloc[0],
        metadata_df["pitch_length"].iloc[0],
    )
    match_date = metadata_df["match_date"].iloc[0].split(" ")[0]
    pitch = VerticalPitch(
        pitch_type="secondspectrum",
        pitch_width=pitch_size[0],
        pitch_length=pitch_size[1],
        line_color="#E2E2E2",
    )

    # we make use of team shirt and number colors provided in the metadata file
    team_shirt_colors = [
        metadata_df["home_team_jersey_color"].iloc[0],
        metadata_df["away_team_jersey_color"].iloc[0],
    ]
    team_number_colors = [
        metadata_df["home_team_number_color"].iloc[0],
        metadata_df["away_team_number_color"].iloc[0],
    ]
    lineups = [home_lineup_df, away_lineup_df]

    fig, ax = pitch.draw(figsize=(pitch_size[0] / 10, pitch_size[1] / 10))

    # The TV broadcast camera view of the pitch (if available) is depicted as a light green polygon.
    corner_coords = []
    for corner in ["top_left", "bottom_left", "bottom_right", "top_right", "top_left"]:
        corner_coords.append((visible_area[f"x_{corner}"], visible_area[f"y_{corner}"]))

    if corner_coords != []:
        pitch.polygon([corner_coords], color="#4c8527", alpha=0.05, ax=ax)

    # Per team, draw the players
    for index, team in enumerate([home_df, away_df]):
        shirt_color = team_shirt_colors[index]
        number_color = team_number_colors[index]
        lineup = lineups[index]
        id2shirt = dict(zip(lineup["player_id"], lineup["player_shirt_number"]))
        for _, player in team.iterrows():
            pnum = id2shirt[player["object_id"]]
            pitch.plot(
                truncate(player["x"], 1),
                truncate(player["y"], 1),
                ax=ax,
                linewidth=0,
                marker="o",
                markersize=20,
                markeredgecolor="black",
                color=shirt_color,
            )

            pitch.annotate(
                pnum,
                xy=(truncate(player["x"], 1), truncate(player["y"], 1)),
                c=number_color,
                va="center",
                ha="center",
                size=11,
                weight="bold",
                ax=ax,
            )

    # Plot the ball
    relative_ball_height = 1 - (
        (max_ball_z - truncate(ball_df["z"].iloc[0], 1)) / max_ball_z
    )
    pitch.plot(
        truncate(ball_df["x"].iloc[0], 1),
        truncate(ball_df["y"].iloc[0], 1),
        ax=ax,
        linewidth=0,
        marker="o",
        markersize=10,
        markeredgecolor="black",
        markerfacecolor=str(max(relative_ball_height, 0)),
    )

    ax.set_title(f"{home} vs. {away} on {match_date}, Half: {half}, Time: {match_time}")

    # Save the figure
    fig.savefig(store_path / f"{frame_id}.png", dpi=160)
    plt.close(fig)
