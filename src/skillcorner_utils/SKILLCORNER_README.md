# Information about the data provided by Skillcorner

## Lineup table
- Player `id`, `name`, `position`, `birthdate`: player information
- Player `game time`: time player played in the game
- Player `yellow/red card`: number of yellow/red cards received
- Player `is_injured`: whether the player is injured
- Player `(own) goals scored`: number of goals scored (a second column for own goals)

## Metadata table
- Game `home` and `away` teams
- Jersey and number `color` of the teams
- Team coaches `name`
- Stadium `name` and `length/width`
- Data FPS

## Play direction table
- The `play_direction` for each `team` based on the `half`

## Off-ball runs table ([more info](https://skillcorner.crunch.help/en/glossaries/running-off-ball-metrics-1))
### Glossary:
- `threat` ([more info](https://skillcorner.crunch.help/en/models-general-concepts/threat)): 
[0, 1] value, attacking team scoring a goal in next 10 seconds

### Columns:
- `quality_check` for double check?
- Player `id`, `name`, etc.
- Player `minutes_played_per_match` played in the game
- `adjusted_min_tip_per_match`: minutes played when the team is in possession of the ball
- Player `count_runs_per_match` (took more than 0.7 sec and reached above 15 km/h when the team 
is in possession of the ball)
- Player `count_dangerous_runs_per_match`
- Player threat coefficient sum for the match: `runs_threat_per_match`
- Player `count_runs_leading_to_goal_per_match` counts the runs that lead to a goal in next 10 
seconds
- Player `count_runs_targeted_per_match` counts the runs that were target of a pass
- Player `count_runs_received_per_match` counts the runs that received a pass at the end
- Player `count_runs_leading_to_shot_per_match` counts the runs that lead to a shot (whether 
or not by him) in next 10 seconds
- `runs_targeted_threat_per_match` is the sum of the threat coefficient of the runs that were
target of a pass
- `runs_received_threat_per_match` is the sum of the threat coefficient of the runs that
received a pass at the end
- `count_dangerous_runs_targeted_per_match` counts the dangerous runs that lead were target of
a pass
- `count_dangerous_runs_received_per_match` counts the dangerous runs that received a pass at
the end

## Phase table
- `start` and `end` frame IDs of `IN_POSSESSION` and `OUT_POSSESSION` teams.
    - `IN_POSSESSION` is when the team has the ball
    - `OUT_POSSESSION` is when the team does not have the ball and seeks to regain it

## Physical table ([more info](https://skillcorner.crunch.help/en/glossaries/physical-data-glossary))
### Glossary:
- `player_id`, `player_name`, etc.: identifies the player
- `1/2` suffix: indicates the first or second half. If no suffix, it is the total for the game
- `Sprint`: Speed above 25 km/h
- `HSR`: Speed between 20 and 25 km/h (HSR: High Speed Run)
- `Running`: Speed between 15 and 20 km/h
- `High Acceleration`: Acceleration above 3 m/s²
- `High Deceleration`: Deceleration below -3 m/s²
- `Medium Acceleration`: Acceleration between 1.5 m/s² and 3 m/s²
- `Minutes`: Time played in the game
- `TIP`: Team in possession of the ball
- `OTIP`: Opponent team in possession of the ball

- For a run to count, it must last for at least 0.7s!

### Columns:
- `Distance`: total distance covered by the player
- `Distance 1/2`: distance covered in the first/second half
- `Sprinting Distance`: distance covered at a speed above 25 km/h
- `Sprinting Distance 1/2`: distance covered at a speed above 25 km/h in the first/second half
- `HSR Distance`: distance covered at a speed between 20 and 25 km/h
- `HSR Distance 1/2`: distance covered at a speed between 20 and 25 km/h in the first/second half
- `Running Distance`: distance covered at a speed between 15 and 20 km/h
- `Running Distance 1/2`: distance covered at a speed between 15 and 20 km/h in the first/second
half
- `Count HSR`: number of high speed runs
- `Count HSR 1/2`: number of high speed runs in the first/second half
- `Count Sprints`: number of sprints
- `Count Sprints 1/2`: number of sprints in the first/second half
- `Count Medium Acceleration`: number of accelerations between 1.5 m/s² and 3 m/s² (**needs to last
for at least 0.7s**)
- `Count Medium Acceleration 1/2`: number of accelerations between 1.5 m/s² and 3 m/s² in the
first/second half
- `Count High Acceleration`: number of accelerations above 3 m/s²
- `Count High Acceleration 1/2`: number of accelerations above 3 m/s² in the first/second half
- `Count High Deceleration`: number of decelerations below -3 m/s²
- `Count High Deceleration 1/2`: number of decelerations below -3 m/s² in the first/second half
- `Distance TIP`: distance covered when the team is in possession of the ball
- `Distance TIP 1/2`: distance covered when the team is in possession of the ball
in the first/second
- `Sprinting Distance TIP`: distance covered at a speed above 25 km/h when the team is in
possession of the ball
- `Sprinting Distance TIP 1/2`: distance covered at a speed above 25 km/h when the team is in
- ... (quite a few more columns, see the glossary for more info!)

- `PSV-99`: peak sprint velocity 99th percentile, reflecting peak speed of a player sustaining
it long enough
- `Minutes TIP`: time played when the team is in possession of the ball
- `Minutes TIP 1/2`: time played when the team is in possession of the ball in the first/second half
- `Minutes OTIP`: time played when the opponent team is in possession of the ball
- ... (a few more columns, see the glossary for more info!)

## On-ball pressures table ([more info](https://skillcorner.crunch.help/en/glossaries/overcoming-pressure))
### Glossary:
- `loss`: incomplete pass, lost duel, bad touch turning over possession or ball going out of play
- `ball retention`: successful pass or keep possession of the ball
- `dangerous pass`: pass that leads to a goal in next 10 seconds (Skillcorner xThreat model)
- `difficult pass`: pass that has less than 65% chance of being successful
(Skillcorner xPass model)

### Columns:
- `player_id`, `player_name`, etc.: identifies the player
- `minutes_played_per_match`: time player played in the game
- `adjusted_min_tip_per_match`: minutes played when the team is in possession of the ball

#### Ball loss and ball retention under pressure metrics:
- `count_pressures_received_per_match`: number of times the player was under pressure
- `count_forced_losses_under_pressure_per_match`: number of times the player lost the ball under
- `count_ball_retentions_under_pressure_per_match`: number of times the player kept
posession of the ball under pressure

#### Pass success metrics under pressure metrics:
- `count_pass_attempts_under_pressure_per_match`: number of pass attempts under pressure
- `count_completed_passes_under_pressure_per_match`: number of successful passes under pressure
- `ball_retention_ratio_under_pressure`: ratio of successful ball retentions under pressure
(`count_ball_retentions_under_pressure_per_match` / `count_pressures_received_per_match`)
- `pass_completion_ratio_under_pressure`: ratio of successful passes under pressure
(`count_completed_passes_under_pressure_per_match` / `count_pass_attempts_under_pressure_per_match`)

#### Dangerous pass and difficult pass under pressure metrics:
- `count_dangerous_pass_attempts_under_pressure_per_match`: number of dangerous pass attempts
- `count_completed_dangerous_passes_under_pressure_per_match`: number of successful dangerous
passes
- `dangerous_pass_completion_ratio_under_pressure`: ratio of successful dangerous passes under
pressure
- `count_difficult_pass_attempts_under_pressure_per_match`: number of difficult pass attempts
- `count_difficult_pass_attempts_under_pressure_per_match`: number of successful difficult passes
- `difficult_pass_completion_ratio_under_pressure`: ratio of successful difficult passes under
pressure

## Passes table ([more info](https://skillcorner.crunch.help/en/glossaries/passing-metrics-1))
- Player information: `player_id`, `player_name`, etc.
- `minutes_played_per_match`: time player played in the game
- `adjusted_min_tip_per_match`: minutes played when the team is in possession of the ball
- `count_opportunities_to_pass_to_runs_per_match`: number of opportunities to pass to runs
- `count_pass_attempts_to_runs_targeted_per_match`: number of actual pass attempts to runs
- `pass_opportunities_to_runs_threat_per_match`: sum of the threat coefficient of the opportunities
- `runs_to_which_pass_attempted_threat_per_match`: sum of the threat coefficient of the actual
pass attempts
- `pass_completion_ratio_to_runs`: ratio of successful passes to runs
- `count_runs_by_teammate_per_match`: number of runs made by the player's teammates when he was
in possession of the ball
- `runs_to_which_pass_completed_threat_per_match`: sum of the threat coefficient of the runs
that successfully received a pass
- `count_completed_pass_to_runs_per_match`: number of successful passes to runs
- `count_completed_pass_to_runs_leading_to_shot_per_match`: number of successful passes to runs
that lead to a shot in next 10 seconds
- `count_completed_pass_to_runs_leading_to_goal_per_match`: number of successful passes to runs
that lead to a goal in next 10 seconds
- `count_pass_opportunities_to_dangerous_runs_per_match`: number of opportunities to pass to
dangerous runs
    - dangerous runs are specified by the Skillcorner models, must have over 2% chance to lead
    to a goal within 10 seconds
- `count_pass_attempts_to_dangerous_runs_per_match`: number of actual pass attempts to dangerous
runs
- `count_completed_pass_to_dangerous_runs_per_match`: number of successful passes to dangerous
runs

## Tracking table
- `half`: first or second half
- `frame_id`: frame number
- `timestamp`: multiples of 100, the scale is {second * 1_000}
- `object_id`: the id of the tracked object
    - $-1$: the ball
    - $otherwise$: the player_id
- `x`: meter scale from the center of the field, the long side of the field
(the center of the field is 0)
- `y`: meter scale from the center of the field, the short side of the field
(the center of the field is 0)
- `z`: meter scale from ground (ground level is 0)
- `extrapolated`: whether the position is extrapolated (not visible in the frame but estimated
by the Skillcorner models)

## Visible area table
- `x_top_left`: x-coordinate of left top corner of the visible area
- `y_top_left`: y-coordinate of left top corner of the visible area (same as `y_top_right`)
- `x_top_right`: x-coordinate of right top corner of the visible area
- `y_top_right`: y-coordinate of right top corner of the visible area (same as `y_top_left`)
- `x_bottom_right`: x-coordinate of right bottom corner of the visible area
- `y_bottom_right`: y-coordinate of right bottom corner of the visible area
(same as `y_bottom_left`)
- `x_bottom_left`: x-coordinate of left bottom corner of the visible area
- `y_bottom_left`: y-coordinate of left bottom corner of the visible area
(same as `y_bottom_right`)