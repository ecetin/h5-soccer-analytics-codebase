import enum
import json
import shutil
import zipfile
from datetime import datetime
from pathlib import Path
from typing import Dict, Tuple

import pandas as pd
from pydantic import BaseModel
from tqdm import tqdm

from config.config_params import DATA_DIR, ROOT_DIR, SKILLCORNER_DIR
from src.base_utils.base_dataset import BaseDataset
from src.data_loading.data_load_save_utils import list_to_csv
from src.skillcorner_utils.visualization_utils import animate, plot


class Direction(enum.Enum):
    """Enum for the directions of a match."""

    BOTTOM_TO_TOP = "BOTTOM_TO_TOP"
    """Bottom to top direction"""
    TOP_TO_BOTTOM = "TOP_TO_BOTTOM"
    """Top to bottom direction"""


class PhaseNames(enum.Enum):
    """Enum for the phases of a match."""

    IN_POSSESSION = "IN_POSSESSION"
    """Team is in possession of the ball"""
    OUT_POSSESSION = "OUT_POSSESSION"
    """Team is out of possession of the ball"""
    IN_CONTEST = "IN_CONTEST"
    """Team is in contest for the ball"""
    NONE = "NONE"
    """No phase"""


side_dict = {
    "b2t": [Direction.BOTTOM_TO_TOP, Direction.TOP_TO_BOTTOM],  # bottom2top
    "t2b": [Direction.TOP_TO_BOTTOM, Direction.BOTTOM_TO_TOP],  # top2bottom
}


class SkillcornerDataStructure(BaseModel):
    """The data structure for the SkillCorner data."""

    match_id: int
    """SkillCorner ID for the match"""
    folder_path: Path
    """Folder path for the raw SkillCorner data"""
    metadata_file: Path
    """Metadata JSON file path"""
    tracking_file: Path
    """Tracking JSON file path"""
    physical_file: Path
    """Physical JSON file path"""
    passes_file: Path
    """Passes JSON file path"""
    on_ball_pressures_file: Path
    """On ball pressures CSV file path"""
    off_ball_runs_file: Path
    """Off ball runs JSON file path"""


class SkillCorner(BaseDataset):
    """SkillCorner data for a match is stored in a folder. This class includes the tracking
    data coming from SkillCorner and the methods to load, save and process the data.

    Attributes:
        skillcorner_id (int): The SkillCorner match ID.
        skillcorner_data (SkillcornerDataStructure): The SkillCorner data structure containing
            the folder path and the data path.
        metadata (pd.DataFrame): The metadata for the match
        play_direction (pd.DataFrame): The play direction for the match
        tracking (pd.DataFrame): The tracking data for the match
        visible_area (pd.DataFrame): The visible area data for the match
        phase (pd.DataFrame): The phase data for the match
        lineup (pd.DataFrame): The lineup data for the match
        physical (pd.DataFrame): The physical data for the match
        passes (pd.DataFrame): The passes data for the match
        on_ball_pressures (pd.DataFrame): The on ball pressures data for the match
        off_ball_runs (pd.DataFrame): The off ball runs data for the match
        max_ball_z (int): The maximum ball height in meters that we see in our dataset
    """

    fps = 10  # The frame rate of the SkillCorner data
    ball_id = "-1"  # ball_id stored in the output CSV files

    def __init__(
        self, skillcorner_id: int, home_away_date: Tuple[str, str, str]
    ) -> None:
        """Initializes the SkillCorner data for a match.

        Args:
            skillcorner_id (int): The SkillCorner match ID.
            home_away_date (Tuple[str, str, str]): The home team, away team, and the date of the
                match.
        """
        self.skillcorner_data = self.unzip_raw_data_for_match_id(
            skillcorner_id, home_away_date, already_exists=False
        )

        with open(self.skillcorner_data.metadata_file, encoding="utf-8") as f:
            self.metadata = json.load(f)

        try:
            self.load_all_data()

        except FileNotFoundError:
            print(
                f"Skillcorner data for match {skillcorner_id} does not exist at "
                f"{self.skillcorner_data.folder_path}.\n"
                "Calling `save_all_data` first!"
            )
            self.save_all_data()
            self.load_all_data()

        print(f"Skillcorner data for match {skillcorner_id} loaded ...")
        print(
            f"Match: {home_away_date[0]} vs {home_away_date[1]} on {home_away_date[2]}"
        )

    def get_player_ages(self) -> pd.DataFrame:
        """Calculates the age of the players at the time of the match. Adds a column to the
        lineup dataframe and returns the player's age, team name, player id, first name, and
        last name.

        Returns:
            pd.DataFrame: The player's age, team name, player id, first name, and last name.
        """
        # Convert date strings to datetime objects
        self.metadata["match_date"] = pd.to_datetime(self.metadata["match_date"])
        self.lineup["player_birthdate"] = pd.to_datetime(
            self.lineup["player_birthdate"]
        )

        # Calculate age difference in years
        self.lineup["player_age"] = (
            self.metadata["match_date"].iloc[0] - self.lineup["player_birthdate"]
        ).dt.days // 365

        return self.lineup[
            [
                "team_name",
                "player_id",
                "player_first_name",
                "player_last_name",
                "player_age",
            ]
        ]

    def get_goals_and_assists(self):
        """Returns the goals and assists for the match.

        Todo:
            * This function is not implemented yet.
        """
        pass

    def save_all_data(self):
        """Restructures and saves the SkillCorner data into CSV files for quick access later on."""

        if not (self.skillcorner_data.folder_path / "csv_files").exists():
            (self.skillcorner_data.folder_path / "csv_files").mkdir(parents=True)

        self.save_basic_match_info_as_csv(self.skillcorner_data)
        self.save_play_direction_info_as_csv(self.skillcorner_data)
        on_field_object_ids = self.save_tracking_info_as_csv(self.skillcorner_data)
        self.save_lineup_and_player_info_as_csv(
            self.skillcorner_data, on_field_object_ids
        )
        self.save_physical_passes_on_and_off_ball_info_as_csv(self.skillcorner_data)

    def load_all_data(self):
        """Loads the SkillCorner data from the CSV files and stores them in the class."""
        skillcorner_data = self.skillcorner_data
        csv_path = (
            f"{skillcorner_data.folder_path}/csv_files/{skillcorner_data.match_id}"
        )

        self.metadata = pd.read_csv(f"{csv_path}_metadata.csv")
        self.play_direction = pd.read_csv(f"{csv_path}_play_direction.csv")
        self.tracking = pd.read_csv(f"{csv_path}_tracking.csv")
        self.visible_area = pd.read_csv(f"{csv_path}_visible_area.csv")
        self.phase = pd.read_csv(f"{csv_path}_phase.csv")
        self.lineup = pd.read_csv(f"{csv_path}_lineup.csv")
        self.physical = pd.read_csv(f"{csv_path}_physical.csv")
        self.passes = pd.read_csv(f"{csv_path}_passes.csv")
        self.on_ball_pressures = pd.read_csv(f"{csv_path}_on_ball_pressures.csv")
        self.off_ball_runs = pd.read_csv(f"{csv_path}_off_ball_runs.csv")

        # What is the maximum ball height in meters that we see in our dataset
        self.max_ball_z = int(self.tracking.z.max())

    def get_dataframes(self) -> Dict:
        """Returns the dataframes of the SkillCorner data.

        Returns:
            Dict[str, pd.DataFrame]: The dataframes of the SkillCorner data
        """
        return {
            "metadata": self.metadata,
            "play_direction": self.play_direction,
            "tracking": self.tracking,
            "visible_area": self.visible_area,
            "phase": self.phase,
            "lineup": self.lineup,
            "physical": self.physical,
            "passes": self.passes,
            "on_ball_pressures": self.on_ball_pressures,
            "off_ball_runs": self.off_ball_runs,
        }

    def save_basic_match_info_as_csv(self, skillcorner_data: SkillcornerDataStructure):
        """Saves the basic match information as a CSV file.

        Args:
            skillcorner_data (SkillcornerDataStructure): The SkillCorner data structure.
        """
        # basic match info
        match = {}
        match["match_id"] = skillcorner_data.match_id
        match["match_date"] = datetime.strptime(
            self.metadata["date_time"], "%Y-%m-%dT%H:%M:%SZ"
        ).strftime("%m/%d/%Y %H:%M")
        match["competition"] = self.metadata["competition_edition"]["competition"][
            "name"
        ]
        match["season"] = self.metadata["competition_edition"]["season"]["name"]
        match["home_team"] = self.metadata["home_team"]["name"]
        match["away_team"] = self.metadata["away_team"]["name"]
        match["home_score"] = int(self.metadata["home_team_score"])
        match["away_score"] = int(self.metadata["away_team_score"])
        match["home_team_jersey_color"] = self.metadata["home_team_kit"]["jersey_color"]
        match["away_team_jersey_color"] = self.metadata["away_team_kit"]["jersey_color"]
        match["home_team_number_color"] = self.metadata["home_team_kit"]["number_color"]
        match["away_team_number_color"] = self.metadata["away_team_kit"]["number_color"]
        match["home_team_coach"] = (
            f'{self.metadata["home_team_coach"]["first_name"]} {self.metadata["home_team_coach"]["last_name"]}'
        )
        match["away_team_coach"] = (
            f'{self.metadata["away_team_coach"]["first_name"]} {self.metadata["away_team_coach"]["last_name"]}'
        )
        match["pitch_name"] = self.metadata["stadium"]["name"]
        match["pitch_length"] = float(self.metadata["pitch_length"])
        match["pitch_width"] = float(self.metadata["pitch_width"])
        match["provider"] = "SkillCorner"
        match["fps"] = self.fps
        list_to_csv(
            [match],
            skillcorner_data.folder_path
            / "csv_files"
            / f"{skillcorner_data.match_id}_metadata.csv",
        )

        print(f"Metadata {skillcorner_data.match_id} added ...")

    def save_play_direction_info_as_csv(
        self, skillcorner_data: SkillcornerDataStructure
    ):
        """Saves the play direction information as a CSV file.

        Args:
            skillcorner_data (SkillcornerDataStructure): The SkillCorner data structure.
        """
        # play direction part
        if self.metadata["home_team_side"][0] == "left_to_right":
            starting_left_to_right_team = self.metadata["home_team"]["name"]
            starting_right_to_left_team = self.metadata["away_team"]["name"]
        else:
            starting_left_to_right_team = self.metadata["away_team"]["name"]
            starting_right_to_left_team = self.metadata["home_team"]["name"]

        halves = [
            half_number
            for half_number in range(1, len(self.metadata["home_team_side"]) + 1)
        ]
        play_directions = []
        for half in halves:
            play_direction = {}
            play_direction["match_id"] = skillcorner_data.match_id
            play_direction["team_name"] = starting_left_to_right_team
            play_direction["half"] = half
            play_direction["play_direction"] = side_dict["b2t"][(half - 1) % 2].value
            play_directions.append(play_direction)

            play_direction = {}
            play_direction["match_id"] = skillcorner_data.match_id
            play_direction["team_name"] = starting_right_to_left_team
            play_direction["half"] = half
            play_direction["play_direction"] = side_dict["t2b"][(half - 1) % 2].value
            play_directions.append(play_direction)

        list_to_csv(
            play_directions,
            skillcorner_data.folder_path
            / "csv_files"
            / f"{skillcorner_data.match_id}_play_direction.csv",
        )
        print(f"PlayDirection {skillcorner_data.match_id} added ...")

    def save_tracking_info_as_csv(self, skillcorner_data: SkillcornerDataStructure):
        """Saves the tracking information as a CSV file.

        Args:
            skillcorner_data (SkillcornerDataStructure): The SkillCorner data structure.
        """
        # extracting tracking data
        on_field_object_ids = set()

        tracking_list = []
        visible_area_list = []
        phase_list = []
        base_timestamp = 0
        pre_half = 1
        pre_possesion = None
        start_frame_id = None
        pre_frame_id = None

        with open(skillcorner_data.tracking_file, "r", encoding="utf-8") as f:
            for line in f:
                json_object = json.loads(line)
                if json_object["player_data"] != []:
                    if not start_frame_id:
                        start_frame_id = json_object["frame"]
                        pre_possesion = json_object["possession"]["group"]
                        pre_frame_id = start_frame_id
                    possession = json_object["possession"]["group"]
                    frame_id = json_object["frame"]
                    timestamp = json_object["timestamp"]

                    time_object = datetime.strptime(
                        json_object["timestamp"], "%H:%M:%S.%f"
                    )
                    timestamp = (
                        (time_object.hour * 60 + time_object.minute) * 60
                        + time_object.second
                    ) * 1000 + time_object.microsecond // 1000

                    half = json_object["period"]
                    if half != pre_half:
                        base_timestamp = timestamp

                    frame = {}
                    frame["match_id"] = skillcorner_data.match_id
                    frame["half"] = half
                    frame["frame_id"] = frame_id  # frame_id is unique accross the match
                    frame["timestamp"] = (
                        timestamp - base_timestamp
                    )  # timestamp starts from each half start and is in ms
                    frame["object_id"] = SkillCorner.ball_id
                    frame["x"] = json_object["ball_data"]["x"]
                    frame["y"] = json_object["ball_data"]["y"]
                    frame["z"] = json_object["ball_data"]["z"]
                    frame["extrapolated"] = not json_object["ball_data"][
                        "is_detected"
                    ]  # Whether this player's coordinates are extrapolated
                    tracking_list.append(frame)

                    # Store the polygon coordinates of the TV broadcast camera view per frame
                    visible_area = {}
                    visible_area["match_id"] = skillcorner_data.match_id
                    visible_area["frame_id"] = frame_id
                    for key in [
                        "x_top_left",
                        "y_top_left",
                        "x_bottom_left",
                        "y_bottom_left",
                        "x_bottom_right",
                        "y_bottom_right",
                        "x_top_right",
                        "y_top_right",
                    ]:
                        visible_area[key] = json_object["image_corners_projection"][key]

                    visible_area_list.append(visible_area)

                    for obj in json_object["player_data"]:
                        if obj["player_id"] not in on_field_object_ids:
                            on_field_object_ids.add(obj["player_id"])
                        frame = {}
                        frame["match_id"] = skillcorner_data.match_id
                        frame["half"] = half
                        frame["frame_id"] = (
                            frame_id  # frame_id is unique accross the match
                        )
                        frame["timestamp"] = (
                            timestamp - base_timestamp
                        )  # timestamp starts from each half start and is in ms
                        frame["object_id"] = obj["player_id"]
                        frame["x"] = obj["x"]
                        frame["y"] = obj["y"]
                        frame["z"] = 0.0
                        frame["extrapolated"] = not obj["is_detected"]
                        tracking_list.append(frame)

                    # Store in and out of possession phases per team
                    if possession != pre_possesion and half == pre_half:
                        if pre_possesion:
                            phase = {}
                            phase["match_id"] = skillcorner_data.match_id
                            phase["half"] = half
                            phase["team_name"] = (
                                self.metadata["home_team"]["name"]
                                if pre_possesion == "home team"
                                else self.metadata["away_team"]["name"]
                            )
                            phase["name"] = PhaseNames.IN_POSSESSION.value
                            phase["start"] = int(start_frame_id)
                            phase["end"] = int(pre_frame_id)
                            phase_list.append(phase)

                            phase = {}
                            phase["match_id"] = skillcorner_data.match_id
                            phase["half"] = half
                            phase["team_name"] = (
                                self.metadata["away_team"]["name"]
                                if pre_possesion == "home team"
                                else self.metadata["home_team"]["name"]
                            )
                            phase["name"] = PhaseNames.OUT_POSSESSION.value
                            phase["start"] = int(start_frame_id)  # frame_id start
                            phase["end"] = int(pre_frame_id)  # frame_id end
                            phase_list.append(phase)
                        if possession:
                            start_frame_id = frame_id
                    pre_possesion = possession
                    pre_frame_id = frame_id
                    pre_half = half

        list_to_csv(
            tracking_list,
            skillcorner_data.folder_path
            / "csv_files"
            / f"{skillcorner_data.match_id}_tracking.csv",
        )
        print(f"Tracking {skillcorner_data.match_id} added ...")

        list_to_csv(
            visible_area_list,
            skillcorner_data.folder_path
            / "csv_files"
            / f"{skillcorner_data.match_id}_visible_area.csv",
        )
        print(f"Visible Area {skillcorner_data.match_id} added ...")

        list_to_csv(
            phase_list,
            skillcorner_data.folder_path
            / "csv_files"
            / f"{skillcorner_data.match_id}_phase.csv",
        )
        print(f"Phase {skillcorner_data.match_id} added ...")

        return on_field_object_ids

    def save_lineup_and_player_info_as_csv(
        self, skillcorner_data: SkillcornerDataStructure, on_field_object_ids: set
    ):
        """Saves the lineup and player information as a CSV file.

        Args:
            skillcorner_data (SkillcornerDataStructure): The SkillCorner data structure.
            on_field_object_ids (set): The object ids of the players on the field.
        """
        # Store the lineups and other player-related information
        lineups = []
        for player in self.metadata["players"]:
            if player["id"] not in on_field_object_ids:
                continue
            lineup = {}
            lineup["match_id"] = skillcorner_data.match_id
            lineup["team_name"] = (
                self.metadata["home_team"]["name"]
                if player["team_id"] == self.metadata["home_team"]["id"]
                else self.metadata["away_team"]["name"]
            )
            lineup["player_id"] = player["id"]
            lineup["player_first_name"] = player["first_name"]
            lineup["player_last_name"] = player["last_name"]
            lineup["player_shirt_number"] = player["number"]
            lineup["player_position"] = player["player_role"]["name"]
            lineup["player_birthdate"] = player["birthday"]
            lineup["start_time"] = player["start_time"]
            lineup["end_time"] = player["end_time"]
            lineup["yellow_card"] = player["yellow_card"]
            lineup["red_card"] = player["red_card"]
            lineup["injured"] = player["injured"]
            lineup["goal"] = player["goal"]
            lineup["own_goal"] = player["own_goal"]
            lineups.append(lineup)

        list_to_csv(
            lineups,
            skillcorner_data.folder_path
            / "csv_files"
            / f"{skillcorner_data.match_id}_lineup.csv",
        )
        print(f"Lineup {skillcorner_data.match_id} added ...")

    def save_physical_passes_on_and_off_ball_info_as_csv(
        self, skillcorner_data: SkillcornerDataStructure
    ):
        """Saves the physical, passes, on ball pressures, and off ball runs information as CSV
        files.

        Args:
            skillcorner_data (SkillcornerDataStructure): The SkillCorner data structure.
        """
        # Convert physical_file, passes_file, on_ball_pressures_file, and
        # off_ball_runs_file json files into CSVs
        for file in [
            skillcorner_data.physical_file,
            skillcorner_data.passes_file,
            skillcorner_data.on_ball_pressures_file,
            skillcorner_data.off_ball_runs_file,
        ]:
            pd.read_json(file, encoding="utf-8").to_csv(
                skillcorner_data.folder_path / "csv_files" / f"{file.stem}.csv",
                encoding="utf-8",
                index=False,
            )
            file_name = (
                file.stem.split("_")[1]
                if len(file.stem.split("_")) == 2
                else "_".join(file.stem.split("_")[1:])
            )
            print(f"{file_name} {skillcorner_data.match_id} added ...")

    def unzip_raw_data_for_match_id(
        self,
        skillcorner_id: int,
        home_away_date: Tuple[str, str, str],
        already_exists: bool = False,
    ) -> SkillcornerDataStructure:
        """Skillcorner data for each match has multiple files. This function unzips the
        skillcorner data for a given match_id and returns the file paths.

        Args:
            skillcorner_id (int): The SkillCorner match ID.
            home_away_date (Tuple[str, str, str]): The home team, away team, and the date of the
                match.
            already_exists (bool): Whether the data already exists in the JSON raw format or not.

        Returns:
            SkillcornerDataStructure: The SkillCorner data structure containing the folder path
                and the data path for JSON files.
        """
        home, away, date = home_away_date
        match_path = Path(f"{DATA_DIR}/{home}_{away}_{date}")

        # Create the data_dir if it does not exist
        skillcorner_json_file_path = Path(f"{match_path}/skillcorner/json_files")
        if not skillcorner_json_file_path.exists():
            skillcorner_json_file_path.mkdir(parents=True)

        # skillcorner zip file path
        zip_file = SKILLCORNER_DIR / f"{skillcorner_id}.zip"

        # Extract the zip file
        if not already_exists:
            with zipfile.ZipFile(zip_file, "r") as zip_ref:
                zip_ref.extractall(skillcorner_json_file_path)
                print(f"Skillcorner data for match {skillcorner_id} unzipped ...")
        else:
            print(
                f"Skillcorner data for match {skillcorner_id} is assumed to already exist "
                f"at {skillcorner_json_file_path}!"
            )

        # match metadata and tracking data file paths
        match_skillcorner_data = SkillcornerDataStructure(
            match_id=skillcorner_id,
            folder_path=skillcorner_json_file_path.parent,
            metadata_file=skillcorner_json_file_path / f"{skillcorner_id}.jsonl",
            tracking_file=skillcorner_json_file_path
            / f"{skillcorner_id}_tracking_extrapolated.jsonl",
            physical_file=skillcorner_json_file_path
            / f"{skillcorner_id}_physical.json",
            passes_file=skillcorner_json_file_path / f"{skillcorner_id}_passes.json",
            on_ball_pressures_file=skillcorner_json_file_path
            / f"{skillcorner_id}_on_ball_pressures.json",
            off_ball_runs_file=skillcorner_json_file_path
            / f"{skillcorner_id}_off_ball_runs.json",
        )

        print(f"-- See path: {skillcorner_json_file_path}")
        return match_skillcorner_data

    def visualize(
        self,
        f_start: int | None = None,
        num_frames: int | None = 1000,
        delete_images_at_end: bool = True,
    ):
        """Creates an animation of the game for visualization with camera view and players.
        The unseen players are extrapolated by Skillcorner. The animation is saved as an mp4 file
        in `h5_data/{match}/skillcorner/visualizations` folder.

        Args:
            f_start (int | None): The starting frame id for the animation. If None, the first
                frame id is used.
            num_frames (int | None): The number of frames to animate. If None, 1000 frames are
                animated.
            delete_images_at_end (bool): Whether to delete the images after the animation is
                created or not.
        """
        home = self.metadata["home_team"].iloc[0]
        away = self.metadata["away_team"].iloc[0]
        date = self.metadata["match_date"].iloc[0]
        date = "-".join(date.split(" ")[0].split("/"))

        visualizations_dir = self.skillcorner_data.folder_path / "visualizations"
        img_dir = Path(visualizations_dir / "img")

        # Remove the match directory if it exists
        if visualizations_dir.exists():
            shutil.rmtree(visualizations_dir, ignore_errors=True)

        # Create the match directory to store the results
        visualizations_dir.mkdir(parents=True)
        img_dir.mkdir()

        animate_fps = int(self.metadata["fps"].iloc[0])
        ball_df = self.tracking[self.tracking.object_id == int(SkillCorner.ball_id)]
        frames_df = self.tracking[self.tracking.object_id != int(SkillCorner.ball_id)]

        if f_start is None:
            f_start = ball_df.frame_id.min()  # start frame_id

        if num_frames is None:
            f_end = f_start + 1000  # end frame_id
        else:
            f_end = f_start + num_frames

        print(f"Selected frame ids to animate is from {f_start} to {f_end}.")

        selected_ball_df = ball_df[
            (ball_df.frame_id >= f_start) & (ball_df.frame_id <= f_end)
        ]

        home_lineup_df = self.lineup[self.lineup.team_name == home]
        home_frames_df = frames_df[
            frames_df.object_id.isin(home_lineup_df.player_id.unique())
        ]

        away_lineup_df = self.lineup[self.lineup.team_name == away]
        away_frames_df = frames_df[
            frames_df.object_id.isin(away_lineup_df.player_id.unique())
        ]

        print(f"{home} vs. {away} on {str(date).split(' ')[0]} ...")
        for frame_id in tqdm(selected_ball_df.frame_id.unique()):
            home_df = home_frames_df[home_frames_df.frame_id == frame_id]
            away_df = away_frames_df[away_frames_df.frame_id == frame_id]
            ball = ball_df[ball_df.frame_id == frame_id]
            timestamp = list(ball.timestamp)[0]
            half = list(ball.half)[0]
            visible_area = (
                self.visible_area[self.visible_area.frame_id == frame_id]
                .drop(columns=["match_id", "frame_id"])
                .iloc[0]
            )
            plot(
                img_dir,
                frame_id,
                timestamp,
                half,
                self.metadata,
                ball,
                self.max_ball_z,
                home_df,
                away_df,
                home_lineup_df,
                away_lineup_df,
                home,
                away,
                visible_area,
            )

        animate(
            img_dir,
            f_start,
            num_frames,
            animate_fps,
            delete_images=delete_images_at_end,
        )
