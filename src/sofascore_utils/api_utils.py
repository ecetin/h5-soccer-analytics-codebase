import requests


def sofascore_api_call(sofascore_match_id: int, api_endpoint_name: str):
    """Makes the sofascore api calls to retrieve information.

    Args:
        sofascore_match_id (int): The sofascore match.
        api_endpoint_name (str): The name of the api endpoint to call.

    Returns:
        json: The json response from the api call.

    Todo:
        * The list of available end points is not exhaustive.
    """
    available_endpoints = [
        "shotmap",
        "statistics",
        "lineups",
        "best-players",
        "managers",
        "h2h",
        "incidents",
        "pregame-form",
        "graph",
        "team-streaks",
        "average-positions",
    ]
    assert api_endpoint_name in available_endpoints, (
        "Invalid API endpoint name." + f"Available endpoints: {available_endpoints}"
    )

    headers = {
        "authority": "api.sofascore.com",
        "accept": "*/*",
        "accept-language": "en-US,en;q=0.9",
        "cache-control": "max-age=0",
        "dnt": "1",
        "if-none-match": 'W/"4bebed6144"',
        "origin": "https://www.sofascore.com",
        "referer": "https://www.sofascore.com/",
        "sec-ch-ua": '"Not.A/Brand";v="8", "Chromium";v="114"',
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": '"macOS"',
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-site",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36",
    }

    response = requests.get(
        f"https://api.sofascore.com/api/v1/event/{sofascore_match_id}/{api_endpoint_name}",
        headers=headers,
    )
    json_response = response.json()
    return json_response
