import zipfile
from pathlib import Path
from typing import Tuple

import pandas as pd
from pydantic import BaseModel

from config.config_params import DATA_DIR, WYSCOUT_DIR
from src.base_utils.base_dataset import BaseDataset
from src.wyscout_utils.file_conversion_utils import wyscout_json_to_df


class WyscoutDataStructure(BaseModel):
    match_id: int
    """Wyscout ID for the match"""
    folder_path: Path
    """Folder path for the raw Wyscout data"""
    data_path: Path
    """Folder path to save the CSV files for the Wyscout data"""


class Wyscout(BaseDataset):
    """Wyscout data for a match is stored in a folder. This class includes the event
    data coming from Wyscout and the methods to load, save and process the data.

    Attributes:
        wyscout_id (int): The Wyscout match id.
        home_team (str): The home team name
        away_team (str): The away team
        match_date (str): The date of the match.
        wyscout_data (WyscoutDataStructure): The Wyscout data structure containing the folder
            path and the data path
        events_df (pd.DataFrame): The events data for the match with lots of columns
    """

    def __init__(self, wyscout_id: int, home_away_date: Tuple[str, str, str]):
        """Initializes the Wyscout object with the match id and the home, away and date.

        Args:
            wyscout_id (int): The Wyscout match id.
            home_away_date (Tuple[str, str, str]): The home team, away team and the date
                of the match.
        """
        self.wyscout_id = wyscout_id
        self.home_team, self.away_team, self.match_date = home_away_date
        self.wyscout_data = self.unzip_raw_data_for_match_id(wyscout_id, home_away_date)

        try:
            self.load_all_data()

        except FileNotFoundError:
            print(
                f"Wyscout data for match {wyscout_id} does not exist at "
                f"{self.wyscout_data.folder_path}.\n"
                "Calling `save_all_data` first!"
            )
            self.events_df = wyscout_json_to_df(self.wyscout_data.data_path)
            self.save_all_data()
            self.load_all_data()

        print(f"Skillcorner data for match {wyscout_id} loaded ...")
        print(
            f"Match: {home_away_date[0]} vs {home_away_date[1]} on {home_away_date[2]}"
        )

    def save_all_data(self) -> None:
        """Saves the Wyscout data into CSV files for quick access later on."""
        csv_path = self.wyscout_data.folder_path / "csv_files"
        wyscout_id = self.wyscout_data.match_id
        if not csv_path.exists():
            csv_path.mkdir(parents=True)

        self.events_df.to_csv(csv_path / f"{wyscout_id}_events.csv", index=False)

    def load_all_data(self) -> None:
        self.events_df = pd.read_csv(
            self.wyscout_data.folder_path
            / "csv_files"
            / f"{self.wyscout_data.match_id}_events.csv"
        )

    def get_dataframes(self):
        """Returns the dataframes for the match."""
        return {
            "events": self.events_df,
        }

    def unzip_raw_data_for_match_id(
        self,
        wyscout_id: int,
        home_away_date: Tuple[str, str, str],
        already_exists: bool = False,
    ) -> WyscoutDataStructure:
        """Wyscout data for each match has multiple files.
        This function unzips the wyscout data for a given match_id and
        returns the file paths.

        Args:
            wyscout_id (int): The Wyscout match ID.
            home_away_date (Tuple[str, str, str]): The home team, away team and the date
                of the match.
            already_exists (bool, optional): If True, the function assumes that the data
                already exists and does not unzip the files again. Defaults to False.

        Returns:
            WyscoutDataStructure: The Wyscout data structure containing the Wyscout match ID,
            folder path and the data path
        """
        home, away, date = home_away_date
        match_path = Path(f"{DATA_DIR}/{home}_{away}_{date}")

        # Create the data_dir if it does not exist
        wyscout_json_file_path = Path(f"{match_path}/wyscout/json_files")
        if not wyscout_json_file_path.exists():
            wyscout_json_file_path.mkdir(parents=True)

        # wyscout zip file path
        zip_file = WYSCOUT_DIR / f"{wyscout_id}.zip"

        # Extract the zip file
        if not already_exists:
            with zipfile.ZipFile(zip_file, "r") as zip_ref:
                zip_ref.extractall(wyscout_json_file_path)
            print(f"Wyscout data for match {wyscout_id} unzipped ...")
        else:
            print(
                f"Wyscout data for match {wyscout_id} is assumed to already exist "
                f"at {wyscout_json_file_path}!"
            )

        match_wyscout_data = WyscoutDataStructure(
            match_id=wyscout_id,
            folder_path=wyscout_json_file_path.parent,
            data_path=wyscout_json_file_path / f"{wyscout_id}.json",
        )

        print(f"-- See path: {wyscout_json_file_path}")

        return match_wyscout_data
