import json
from pathlib import Path

import pandas as pd


def wyscout_json_to_df(file_path: Path):
    """This function reads the wyscout_eventdata (JSON file) and converts it to a
    pandas DataFrame. The Wyscout data consists of
    - A meta part (which is empty)
    - An events part.
    We are interested in the events part of the DataFrame.

    Args:
        file_path (Path): The path to the {wyscout_id}.json file.

    Returns:
        pd.DataFrame: The events data for the match with lots of columns.
    """
    with open(file_path, encoding="utf8") as f:
        js = json.load(f)
        df = pd.json_normalize(js["events"])
    return df
