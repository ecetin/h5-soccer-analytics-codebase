from typing import List

import pandas as pd
from pydantic import BaseModel, ConfigDict

from src.base_utils.base_dataset import RetrievedColumn
from src.skillcorner_utils import SkillCorner
from src.wyscout_utils import Wyscout


class MatchData(BaseModel):
    """This class represents a match and contains the data from Skillcorner and Wyscout."""

    model_config = ConfigDict(arbitrary_types_allowed=True)

    home: str
    """The home team name"""
    away: str
    """The away team name"""
    date: str
    """The date of the match"""
    skillcorner: SkillCorner | None
    """The Skillcorner data for the match"""
    wyscout: Wyscout | None
    """The Wyscout data for the match"""

    def __str__(self) -> str:
        """Returns a string representation of the match.

        Returns:
            str: A string representation of the match.
        """
        return (
            f"{self.home} vs {self.away} on {self.date}\n"
            f"- SkillCorner data exists: {self.skillcorner is not None}\n"
            f"- Wyscout data exists: {self.wyscout is not None}"
        )

    def get_all_dataframes(self) -> dict[str, pd.DataFrame]:
        """Returns the dataframes for the match.

        Returns:
            dict[str, pd.DataFrame]: A dictionary containing the dataframes for the match.
            The keys are the names of the dataframes and the values are the dataframes themselves.
        """
        all_dataframes = {}

        if self.skillcorner:
            for k, v in self.skillcorner.get_dataframes().items():
                all_dataframes[k] = v

        if self.wyscout:
            for k, v in self.wyscout.get_dataframes().items():
                all_dataframes[k] = v

        return all_dataframes

    def get_columns_with_string(self, string: str) -> List["RetrievedColumn"]:
        """Returns the columns that contain the given string.

        Args:
            string (str): The string to search for in the column names.

        Returns:
            List[RetrievedColumn]: A list of RetrievedColumn objects containing the column name,
                the provider and the dataframe name.
        """
        dataframe_column_list = []

        if self.skillcorner:
            dataframe_column_list += self.skillcorner.get_columns_with_string(string)

        if self.wyscout:
            dataframe_column_list += self.wyscout.get_columns_with_string(string)

        return dataframe_column_list

    def get_player_ages(self) -> pd.DataFrame:
        """Retrieve the ages of the players in the match.

        Returns:
            pd.DataFrame: Dataframe for the age of players in the match.
        """
        if self.skillcorner:
            return self.skillcorner.get_player_ages()
        else:
            print("No Skillcorner data available...")

    @classmethod
    def load_matches(cls, games: pd.DataFrame) -> List["MatchData"]:
        """Loads a list of games into MatchData objects.

        Args:
            games (pd.DataFrame): A dataframe containing the games to load with their
                Wyscout and Skillcorner IDs.

        Returns:
            List[MatchData]: A list of MatchData objects containing the data for the games.
        """
        matches: List["MatchData"] = []

        for _, game in games.iterrows():
            if not isinstance(game["skillcorner"], int):
                print(
                    f"Game {game['home']} vs {game['away']} has no Skillcorner data! "
                    f"{game['skillcorner']} is not a valid Skillcorner ID..."
                )
                skillcorner_data = None
            else:
                skillcorner_data = SkillCorner(
                    skillcorner_id=game["skillcorner"],
                    home_away_date=(game.home, game.away, game.date),
                )

            if not isinstance(game["wyscout"], int):
                print(
                    f"Game {game['home']} vs {game['away']} has no Wyscout data! "
                    f"{game['wyscout']} is not a valid Wyscout ID..."
                )
                wyscout_data = None
            else:
                wyscout_data = Wyscout(
                    wyscout_id=game["wyscout"],
                    home_away_date=(game.home, game.away, game.date),
                )

            matches.append(
                MatchData(
                    home=game.home,
                    away=game.away,
                    date=game.date,
                    skillcorner=skillcorner_data,
                    wyscout=wyscout_data,
                )
            )

        print()
        print(f"Loaded {len(matches)} matches...")

        return matches
