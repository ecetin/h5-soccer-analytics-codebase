from pathlib import Path
from typing import List

import pandas as pd

from config.config_params import ROOT_DIR


def list_to_csv(list: List, path: Path) -> None:
    """This function stores a list of dictionaries into a CSV file.

    Args:
        list (List): The list of dictionaries to be stored into a CSV file.
        path (Path): The path to the CSV file.
    """
    pd.DataFrame(list).to_csv(path, index=False)


def filter_match_ids_for_team(team_name: str):
    """This function filters the match ids for a given team name.

    Args:
        team_name (str): The name of the team to filter the match ids.

    Returns:
        pd.DataFrame: A DataFrame containing the match information for the given team.
    """

    match_ids = pd.read_csv(f"{ROOT_DIR}/data/matchids.csv")
    analyzed_team_home = match_ids[(match_ids["home"] == team_name)]
    analyzed_team_away = match_ids[(match_ids["away"] == team_name)]
    concat_data = pd.concat([analyzed_team_home, analyzed_team_away])

    concat_data["skillcorner"] = pd.to_numeric(
        concat_data["skillcorner"], errors="coerce"
    ).astype("Int64")
    concat_data["wyscout"] = pd.to_numeric(
        concat_data["wyscout"], errors="coerce"
    ).astype("Int64")
    return concat_data
