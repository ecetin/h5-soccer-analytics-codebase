from abc import ABC, abstractmethod
from typing import List

import pandas as pd
from pydantic import BaseModel, ConfigDict


class BaseDataset(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def save_all_data(self):
        pass

    @abstractmethod
    def load_all_data(self):
        pass

    @abstractmethod
    def get_dataframes(self):
        pass

    @abstractmethod
    def unzip_raw_data_for_match_id(self):
        pass

    def get_columns_with_string(self, string: str) -> List["RetrievedColumn"]:
        """Returns the columns that contain the given string.

        Args:
            string (str): The string to search for in the column names.

        Returns:
            List[RetrievedColumn]: A list of RetrievedColumn objects containing the column name,
            the provider and the dataframe name.
        """
        dataframe_column_list = []

        for df_name, df in self.get_dataframes().items():
            for col in df.columns:
                lower_col = col.lower()
                if string.lower() in lower_col:
                    dataframe_column_list.append(
                        RetrievedColumn(
                            column_name=col,
                            provider=self.__class__.__name__,
                            dataframe_name=df_name,
                        )
                    )
        return dataframe_column_list


class RetrievedColumn(BaseModel):
    """The data type for the retrieved columns while debugging and understanding the data.
    The retrieved columns are the columns that contain a given string.
    """

    model_config = ConfigDict(arbitrary_types_allowed=True)

    column_name: str
    """The full name of the column"""
    provider: str
    """The provider of the data (Skillcorner or Wyscout)"""
    dataframe_name: str
    """The name of the dataframe where the column was found"""
