# Group H5 Soccer Analytics'24 Project

Here we thank our team member Eren for building this repository and writing codes before he decided to quit the course.

## Environment Setup

To setup the environment, make sure you have `pipenv` installed. Then, install requirements using

```bash
pipenv install
```


## Folder Structure

**IMPORTANT:** Do not forget to change the `config_params.py` file in the `config` folder 
according to your environment. 
- `ROOT_DIR`: Root directory of our project
- `SKILLCORNER_DIR`: Directory of the `Skillcorner` zip files
- `WYSCOUT_DIR`: Directory of the `Wyscout` zip files
- `DATA_DIR`: Directory of the matches related to our project

The folder structure **HAS TO BE** as following:

```bash
.
├── Pipfile
├── Pipfile.lock
├── README.md
├── config
│   └── config_params.py
├── data
│   ├── eventvideo.zip
│   ├── matchids.csv
│   ├── primer on project data
│   │   ├── presentation.pdf
│   │   ├── presentation.pptx
│   │   ├── ...
│   │   ├── skillcorner.ipynb
│   │   └── wyscout.ipynb
│   ├── primer on project data.zip
│   ├── skillcorner
│   │   ├── 1193645.zip
│   │   ├── 1193707.zip
│   │   ├── ...
│   │   └── 952209.zip
│   └── wyscout
│       ├── 5414103.zip
│       ├── 5414104.zip
│       ├── ...
│       └── 5541745.zip
├── h5_data
│       └── Denmark_Slovenia_2023-11-17
├── notebooks
│   └── EDA_Eren.ipynb
└── src
    ├── data_loading
    │   ├── __init__.py
    │   └── data_load_save_utils.py
    ├── skillcorner_utils
    │   ├── __init__.py
    │   └── visualization_utils.py
    └── wyscout_utils
        ├── __init__.py
        └── file_conversion_utils.py
```

## Library Usage and Data Access

To access the games you want to analyze, you need to first get the match id for `Skillcorner` and
`Wyscout`. To get this information, use following command:

```python
from src.data_loading.data_load_save_utils import filter_match_ids_for_team

# Get the match ids (Wyscout and Skillcorner), home team, away team and date of the game
denmark_games = filter_match_ids_for_team('Denmark')
denmark_games.head()
```

#### Skillcorner Data

```python
from src.skillcorner_utils import SkillcornerData

skillcorner_data = SkillcornerData(
    skillcorner_id,         # Skillcorner id of the game
    home_away_date          # Tuple of (home team, away team, date of the game)
)
```

#### Wyscout Data

```python
from src.wyscout_utils import WyscoutData

wyscout_data = WyscoutData(
    wyscout_id,             # Wyscout id of the game
    home_away_date          # Tuple of (home team, away team, date of the game)
)
```

## Notebooks

Here we link each bullet points in our report to corresponding jupyter notebooks. Run the notebooks and you will get the figures in our report. However, some links may not be correct here because of updates, but there are also direct links to the code in the context, please refer to that to ensure you get the right code.

|Bullet Point|Notebook|
|--|--|
|Team Performance Analysis||
|Goal kick analysis|notebooks/ron_goal_kick.ipynb|
|Possession - Control|notebooks/Posession.ipynb|
|Opening of play|notebooks/OpeninPressing.ipynb|
|Pressing and defence|notebooks/OpeninPressing.ipynb & notebooks/Defense.ipynb|
|Passing Quality & Attack Pattern|notebooks/passes_analysis_skill_corner.ipynb|
|Denmark Off Ball Runs|notebooks/off_ball_runs.ipynb|
|Passes Analysis|Shiduo/passing_mode_analysis.ipynb|
|Pitch Control|notebooks/pitch_control.ipynb|
|Player with ball under pressure|notebooks/under_pressure.ipynb|
|Shot Analysis|Ruizhe/shot_analysis.ipynb|
|Set Pieces Analysis|Ruizhe/set_pieces_analysis.ipynb|

