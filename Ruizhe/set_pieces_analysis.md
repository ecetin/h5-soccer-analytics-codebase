## Set Pieces Analysis

Set pieces are important ways to organize an attack or make a shot. In this part, we analyze different kinds of set pieces to show how Team Denmark play on set pieces.

#### Throw-ins

Throw-ins usually do not result in good chances to score, but it is also meaningful to explore whether our opponents could organize an possession and attack effectively from a throw-in. We collected some data from throw-ins following.

| Throw-in position | Successful Throw-in % | Possession with shot % | Possession with shot on goal % | Possession with goal % |
|---------------|-----|---------------|---|---|
| Forward Zone      |  55.6 |    11.1   |7.41|0.0|
| Midfield    | 50.7 | 14.9   |4.48|1.49|
| Defensive Zone   | 55.5  |    12.9    |4.31|1.72|

We haven't found team Denmark has some special throw-ins strategies compared to other teams. The following possession of throw-ins are mostly depended on receiver's ability to control the ball.

#### Front Free Kicks

After a foul, our oppentions will get a free kick. Free-kicks could be easily divided into three categories according to their position: front, center and back. While free kicks in front are usually good chances to shot directly or cross, free kicks in center are more likely for a team to reorganize their attack and free kicks in back are more similar to goal kicks.

![img6](images\img6.png)

When our oppentents get a free kick in front, there is usually three ways to play it: short pass to reorganize, cross and shot directly. In this part we are more interested in the latter two and drawed a figure. As shown in the legend, the circles are free kick positions and the triangles are the crosses end locations. We connect lines to show the paths of passes. The crosses that lead to goals are marked specially.

We found that six times they tried to shot directly, three of them are in good postions, close to the foul line. But unfortunately, all of them missed. However, four goals were made by in 16 crosses from free kicks. From the cross lines we could find that the crosses are usually aimed at far post and close to goal area. We must be careful when defending the free kicks, especially defending the far post.

In conclusion, for free kicks in front, Denmark players are more willing to cross than shot and the crosses are more threatening than direct shots. Our defenders must pay more attention on their forwards in penalty area. And we notice normmally C. Eriksen is the one to play the goal kick, whatever the flank is. We should notice it if anyone else stands in front of the spot.

#### Corners

Corners are similar to front free kicks, although it's hard to shot directly by corners. Denmark is very good at scoring by corners. It's easy to understand as their players are usually very high and more likely to win the duel. In our accessible dataset, they got 82 corners and 14 of them (17.1%) are turned into goals. Considering that it's not hard to win 5-6 corners in a game, we should organize more effective defense in corners.

![img7](images\img7.png)

Similar to front free kicks, the crosses from corners are likely to go far post, excluding short passes. The targeted players are usually J. Andersen and S. Kjaer, their centre backs. The two players are higher than 1.90 meters. We have to acknowledge that this strategy seems simple but efficient. Our high players, whatever their position should be, must go back and take part in the defense in penalty area on corners.

#### Mid-field Free Kicks

![img9](images\img9.png)

Mid-field free kicks will indicate how Denmark organize their attacks. We can find in this figure that sometimes Denmark play long passes directly from back to front, and such tactic focuses more on the right flank. When it comes to the left flank, short passes are more preferred. It shows that the left flank players are more willing to control the ball and organize the attack from their side, while the right flank players may play in a more simple way: stand in opponents' penalty areas and seek for chances.

It means that we can deal with them in different ways. For example, offside trap may work for Denmark right flank players.

#### Back Free Kicks and Goal Kicks

We put back free kicks and goal kicks in the same category as they are very similar.

![img8](images\img8.png)

This image shows where free kicks and goal kicks usually go. We can note that for short passes, the majority of them will go the left flank. It means our right forwards and right midfielders should push more on back free kicks and goal kicks.
