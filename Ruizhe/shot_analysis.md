## 3. Shot Analysis

Shot analysis could be an important part of our analysis as it provides details for our defense anagnsit their forwards.

#### Who Shots and How their Shot Quality is?

It's easy to count the shot numbers. For shot quality, We use the difference between shot.xg attribute and shots.postShotXg attribute. Smaller difference between shots.postShotXg and shots.xg shows the player made a shot with high quality.  

![img1](images\img1.png)

This figure shows some statistics we are intersted in. Larger red point shows that the player shot more in the matches. We can say most shots of Denmark are done by three players: R. Højlund, J. Wind and J. Maehle. Moreover, there is a red in this graph. Long distance between the upper side point and the redline indicates the player's shots had high quality. So we notice R. Højlund would be a big threat for our defenders.  

Besides that, we can find out that there isn't a superstar in team Denmark who takes most shots. Moreover, they are relatively not great shooters, we can adopt conservative defensive tactics, avoiding fouls in the game.

#### When do they Shot?

We want to know that at which time interval would Denmark be most likely to behave more aggressively and attempt to goal. We need to be more careful at that time.  

![img2](images\img2.png)

This figures shows shot frequencies of Denmark. We find that they are most aggressive around the break. They are more likely to be defensive at the beginning or near the end of match. As a result, it would be a good strategy if we do counterattack around the break and push them after 70 minutes.

#### Where do Shots Go?

In this part, we try to figure out the shots are more likely to going for which part of the nets. That would be something useful for our goalkeeper.

![img3](images\img3.png)

For parts outside the goal, the number means the frequency. For parts inside the goal, the first number means goal frequency and the second means total frequency. Please note that some shots are not considered because they are blocked.

It is a little interesting that Denmark forwards prefer to shot the left side of the goal than the right side. Although this may be subject to randomness, it may reflect their preferred flank or something else. It indicates that our goalkeeper should be more careful of that left parts as they have greater goal possibility.

#### Where do they Shot?

In this part, we want to get the shot map of Denmark. Then we can be more careful when opponents holding the ball at some specific parts of the field.

![img4](images\img4.png)

This shot map differenciate goals and no-goal shots. And larger icon means the shot has higher xg.

We can learn from the map that most shots resulting in goals are made in the penalty area even in goal area. Sometimes they try long distance shots, but the success rate is really low. It makes sense as Denmark forwards tend to be tall and strong (Højlund 1.91m, Wind 1.90m), having an advantage to physical confrontation in penalty area. That means we can play deep even park the bus, allowing them to shot outside the penalty area: their long shots are not threatening.

#### How do they Shot?

There are different secondary type of shots -- after corner, free kick, etc. We want to find the threatening ones of our opponents.

![img5](images\img5.png)

The blue bar shows the percentage of opportunity while the red bar shows the percentage of goal. We should pay more attention on set pieces, and we should not allow them touch in box easily, although the later idea just is a common sense for all football players. Anyway, Denmark is a typical Scandinavian team which rely on body to win physical confrontation against defenders in box and is good at scoring by crosses and set pieces.
