
from pathlib import Path
import os

ROOT_DIR = Path('/Users/erencetin/Desktop/SA')

SKILLCORNER_DIR = Path(os.path.join(ROOT_DIR, 'data/skillcorner'))
WYSCOUT_DIR = Path(os.path.join(ROOT_DIR, 'data/wyscout'))

# data_dir is where the processed raw data will be stored in
DATA_DIR = Path(os.path.join(ROOT_DIR, 'h5_data'))