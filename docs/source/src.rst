src package
===========

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   src.data_loading
   src.skillcorner_utils
   src.wyscout_utils

Module contents
---------------

.. automodule:: src
   :members:
   :undoc-members:
   :show-inheritance:
