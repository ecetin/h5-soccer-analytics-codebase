.. group_h5_serbia_denmark_analyzing_opponent_denmark documentation master file, created by
   sphinx-quickstart on Thu Mar 14 15:04:47 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Group 5 Serbia vs Denmark analysis! We analyze opponent: Denmark...
==============================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
